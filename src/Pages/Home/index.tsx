import "./index.scss"
import 'animate.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCss3, faGitlab, faHtml5, faReact, faSass } from '@fortawesome/free-brands-svg-icons'
import tsLogo from "../../assets/images/ts_logo.svg"

const Home = () => {

  return (
    <div className={"container"}>
      <div className={"introContainer"}>
        <div className={"introWords"}>
          <h1>Hello, I'm</h1>
          <h2>Mike</h2><span className={"fullStop"}></span>
          <div className={"jobTitle"}>
            Front End Developer
          </div>
          <a href={"/projects"}>
            <button className={"button"}>
              Projects
            </button>
          </a>
        </div>
      </div>

      <div className={"cubeContainer"}>
        <div className={"spinningCube"}>
          <div className={"face1"}>
            <FontAwesomeIcon icon={faReact} color={"#5ED4F4"}/>
          </div>
          <div className={"face2"}>
            <FontAwesomeIcon icon={faCss3} color={"#28A4D9"}/>
          </div>
          <div className={"face3"}>
            <FontAwesomeIcon icon={faHtml5} color={"#F06529"}/>
          </div>
          <div className={"face4"}>
            <FontAwesomeIcon icon={faGitlab} color={"#fca326"}/>
          </div>
          <div className={"face5"}>
            <FontAwesomeIcon icon={faSass} color={"#CC6699"}/>
          </div>
          <div className={"face6"}>
            <img src={tsLogo} alt={"Typescript Logo"}/>
          </div>
        </div>
      </div>

    </div>

  )
}

export default Home