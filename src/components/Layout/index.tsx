import './index.scss'
import Sidebar from '../Sidebar'
import { Link, Outlet } from 'react-router-dom'

const Layout = () => {
  return (
    <div className={"App"}>
      <Sidebar/>
      <div className={"page"}>
        <Link to={"/"} className={"homeLink"}>
          <h1>Mike Allen</h1>
          <p>Front End Developer</p>
        </Link>
        <Outlet />
      </div>
    </div>
  )
}

export default Layout