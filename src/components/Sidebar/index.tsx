import './index.scss'
import Logo from '../../assets/images/logo.png'
import { Link, NavLink } from 'react-router-dom'
import { faBars, faCode, faEnvelope, faHome, faUser } from '@fortawesome/free-solid-svg-icons'
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Sidebar = () => {
  return (
    <div className={"nav-bar"}>
      <Link className={'hamburger'} to={'/'}>
        <FontAwesomeIcon icon={faBars} color={"#4d4d4e"}/>
      </Link>
      <nav>
        <NavLink to={"/"}>
          <FontAwesomeIcon icon={faHome} color={"#4d4d4e"}/>
        </NavLink>
        <NavLink to={"/projects"} className={"projects-link"}>
          <FontAwesomeIcon icon={faCode} color={"#4d4d4e"}/>
        </NavLink>
        <NavLink to={"/about"} className={"about-link"}>
          <FontAwesomeIcon icon={faUser} color={"#4d4d4e"}/>
        </NavLink>
        <NavLink to={"/contact"} className={"contact-link"}>
          <FontAwesomeIcon icon={faEnvelope} color={"#4d4d4e"}/>
        </NavLink>
      </nav>
      <ul>
        <li>
          <a target={"_blank"} rel={"noreferrer"} href={"https://www.linkedin.com/in/ohitsmike/"}>
            <FontAwesomeIcon icon={faLinkedin} color={"#4d4d4e"}/>
          </a>
        </li>
        <li>
          <a target={"_blank"} rel={"noreferrer"} href={"https://gitlab.com/Mike_Allen"}>
            <FontAwesomeIcon icon={faGitlab} color={"#4d4d4e"}/>
          </a>
        </li>
      </ul>
    </div>
  )
}

export default Sidebar