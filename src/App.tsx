import React from 'react';
import './App.scss';
import { Routes, Route } from 'react-router-dom';

import Layout from './components/Layout';
import Home from './Pages/Home'
import About from './Pages/About'
import Projects from './Pages/Projects'
import Contact from './Pages/Contact'

function App() {
  return (
      <>
      <Routes>
        <Route path={"/"} element={<Layout/>}>
          <Route index element={<Home/>}/>
          <Route path={"/projects"} element={<Projects/>}/>
          <Route path={"/about"} element={<About/>}/>
          <Route path={"/contact"} element={<Contact/>}/>
        </Route>
      </Routes>
      </>
  );
}

export default App;
